const { con } = require('../config/db')

// Controller for Book availability
exports.checkAvailability = (req, res) => {
    let query = `CALL FindAvailability(?, @return_date);`
    let values = [req.query.name]
    con.query(query, values , function (err, result) {
        console.log(result)
        if (err) {
            return res.status(500).send({ message: "Something went wrong" })
        }

        if (result[0].length == 0) {
            return res.status(404).send({ message: "No record Found" })
        }


        return res.status(200).send({ date: result[0][0].return_date })
    })
}

// Controller for rent amount

class StrategyManager {
    constructor() {
        this._strategy = null;
    }
    set strategy(strategy) {
        this._strategy = strategy
    }
    get strategy() {
        return this._strategy
    }
    doAction(days, expression) {
        console.log(this._strategy)
        return this._strategy.calculateCharge(days, expression)
    }
}

class RegularStrategy {
    calculateCharge(days, expression) {
        expression = expression.replaceAll('${days}', days)
        let price = eval(expression)
        return price
    }
}

class NovelStrategy {

    calculateCharge(days, expression) {
        console.log(expression, "aaaaaaaaaaaaaaaa")
        expression = expression.replaceAll('${days}', days)
        console.log(expression)
        let price = eval(expression)
        return price
    }
}

class FictionStrategy {
    calculateCharge(days, expression) {
        expression = expression.replaceAll('${days}', days)
        let price = eval(expression)
        return price
    }
}

function returnConcreteObject(type) {
    console.log('asdasdasdasd', type)
    switch (type) {
        case 'Regular':
            return new RegularStrategy()
            break
        case 'Fiction':
            return new FictionStrategy()
            break
        case 'Novel':
            return new NovelStrategy()
            break
    }
}

const strategyManager = new StrategyManager()

exports.calculateCharge = (req, res, next) => {

    let query = `CALL GetExpression(?, @days_to_return, @book_type, @expression)`
    let values = [req.params.id]
    con.query(query, values, function (err, result) {
        if (err) {
            console.log(err)
            return res.status(500).send({ message: "Something went wrong" })
        }
        console.log(result[0])
        if (result[0].length == 0) {
            return res.status(404).send({ message: "No record Found" })
        }
        
        // let stObj = 
        strategyManager.strategy = returnConcreteObject(result[0][0]['book_type'])
        let price = strategyManager.doAction(result[0][0]['days_to_return'], result[0][0]['expression'])
        return res.status(200).send({ price: price })
    })
}


