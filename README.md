## Code Setup

1. **Clone the project:**
   ```bash
   git clone https://gitlab.com/suicide11/source-one.git 
   ```
2. **Import the sql dump file in your mysql server**
3. **Install Node dependencies using npm i**
    ```bash
    npm i 
    ```
4. **Now start the server using nodemon or node index.js**
    ```bash
   nodemon or node index.js
   ```



## Approach
The problem can be solved in three different ways. My codebase uses one of the three techniques mentioned below:
1. Update the Controller every time library owner changes the logic for calculating rent price. (Not recommended)
2. Use **Strategy Pattern** to create different Strategy Class based on different types of books and write mathematical functions for each type. We still have to update the class when library owner changes the logic. (This breaks the SOLID principles)
3. We can optimise the above solution by introducing expression rule that will be entered by the admin which will be used to calculate the rent price. (We don't have to modify the class. Rules will be now stored in DB)


## My Solution

I have used the 3rd approch to solve the problem. As asked in the requirements that my code should evolve as it moves through the stories, so I have described my solution story wise.

#### Story 1:
  As per 1st story, I have to return the price by multiplying 1 with days_to_return. So I have directly written a query that takes rent id as input and returns the price

#### Story 2:
  Now we got a problem based on type of book. So I created a new column named type in book table and also introduced a rule table that will hold multiplier (as I am assuming that the admin can change the multiplier any time). This gives us the advantage of dynamic pricing based on multiplier.
    
#### Story 3:
  Now the story changes to a complex conditional and logical expression that needs to be solved inorder to get the rent amount. So, I used the approach of tokenizer and parsing of mathematical equation inorder to make my code work in any given condition without changing the controller until and unless a new book type is introduced. For the time being I have used "eval" function from JS to run the expression due to time constraints. We can use a custom parser as well.
    
  So, in order to achieve this I modified the rule table and changed the multiplier to an expression. Now using a JOIN query, I fetched the expression and replaced the variable with days_to_return. 
    
After completing story 3, my code is robust enough to handle all kinds of expressions that admin wants to be solved to calculate rent value. Also, if a new type is added we just have add rule in rules table and add a concrete class for that type in our controller file.

## Future Scope
  As per my current approach, whenever we change the rule, the rent amount will get calculated using the new rule. But in case we want to update the rule only for users, who rented the book after the rule change, we can achieve it by providing a timestamp to the rule. So, by comparing the rule time with borrowed time, we will get to know the expression we need to use.
    
  We can further optimize it by calculating the rent amount while borrowing the book and storing it in table as we knwo the day of return and the expression we need to calculate the price.
    
  I have a open point when we have a condition of late submission and early submission of book. As this was not mentioned in problem statement I have not covered this case. But we can solve it using different approches. (keeping it open for discussion)
