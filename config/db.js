const mysql = require('mysql');

exports.con = mysql.createConnection({
  host: process.env.HOST ,
  user: process.env.USER,
  password: process.env.PASSWORD,
  database: process.env.DATABASE,
  port: process.env.DBPORT
});

