var express = require('express');
var router = express.Router();
const { calculateCharge, checkAvailability } = require('../controller/index')

router.get('/availability',checkAvailability)
router.get('/rent/:id', calculateCharge)

module.exports = router;
