const request = require("supertest")
const app = require("../index")

describe("GET /rent", () => {
  it("should return the total rent", async () => {
    return request(app)
      .get("/rent/6386")
      .expect('Content-Type', /json/)
      .expect(200)
      .then((res) => {
        expect(res.statusCode).toBe(200);
      })
  });

  it("should return error 404 not found", async () => {
    return request(app)
      .get("/rent/1")
      .expect('Content-Type', /json/)
      .expect(404)
      .then((res) => {
        expect(res.statusCode).toBe(404);
      })
  });
});

describe("GET /availability", () => {
  it("should return date when book will be available", async () => {
    return request(app)
      .get("/availability?name=but that constant")
      .expect('Content-Type', /json/)
      .expect(200)
      .then((res) => {
        expect(res.statusCode).toBe(200);
      })
  });

  it("should return error 404 not found", async () => {
    return request(app)
      .get("/availability?name=error")
      .expect('Content-Type', /json/)
      .expect(404)
      .then((res) => {
        expect(res.statusCode).toBe(404);
      })
  });
});