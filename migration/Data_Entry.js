const mysql = require('mysql');
const { con } = require('../config/db')
var csv = require("csvtojson");
const moment = require('moment')

csv()
    .fromFile('customer_data.csv')
    .then(function (jsonArrayObj) { //when parse finished, result will be emitted here.

        jsonArrayObj.forEach((data, user_id) => {

            let customer_name = data.customer_name
            let userRow = `INSERT INTO user (user_id, name) VALUES ("${user_id}","${customer_name}");`
            con.query(userRow, (err, result) => {
                data['books'].forEach((book, book_id) => {
                    let bookData = {
                        id: user_id * 100 + book_id,
                        book_name: book.book_name,
                        book_author: book.author_name
                    }
                    let bookQuery = `INSERT INTO book (id, author_name, book_name) VALUES ("${bookData.id}","${bookData.book_author}","${bookData.book_name}");`
                    con.query(bookQuery, (err, result) => {
                    })
                    let rent = {
                        book_id: bookData.id,
                        user_id: user_id,
                        author_name: book.author_name,
                        book_name: book.book_name,
                        issue_date: book.lend_date + ' 00:00:00',
                        days_to_return: book.days_to_return,
                        return_date: moment(book.lend_date, "YYYY-MM-DD").add(book.days_to_return, 'days').format("YYYY-MM-DD hh:mm:ss")

                    }
                    let rentQuery = `INSERT INTO rent (user_id, book_id,issue_date,days_to_return, return_date) VALUES ("${rent.user_id}","${rent.book_id}","${rent.issue_date}","${rent.days_to_return}","${rent.return_date}");`
                    con.query(rentQuery, (err, result) => {
                    })
                })
            })
            // sql
            data.books = JSON.parse(data.books)

        })
    })
