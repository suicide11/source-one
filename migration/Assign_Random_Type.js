const mysql = require('mysql');
const { con } = require('../config/db')
function generateRandomType(){
    let bookType = [ 'Regular', 'Fiction' , 'Novel']
    let random = Math.floor(Math.random()*10)
    return bookType[random%3]
}

let fetchAll = `Select id from book`;
let createColumnQuery = `alter table book add book_type ENUM ('Regular', 'Fiction', 'Novel') not null default '${generateRandomType()}';`
con.query(createColumnQuery,(err,result)=>{
    if(!err)
        con.query(fetchAll, (err, result) => {
            if(!err) {
                console.log(result)
                result.forEach(element => {
                    let addTypeColumn = `update book set book_type = '${generateRandomType()}' where id = ${element.id};`
                    con.query(addTypeColumn,(err,result) =>{
                        console.log(err)
                    })
                });
            }
        })
    console.log(err)
})
